#!/bin/sh
##
# Script to deploy a Kubernetes project with a StatefulSet running a MongoDB Replica Set, to GKE.
##
# Define storage class for dynamically generated persistent volumes
# NOT USED IN THIS EXAMPLE AS EXPLICITLY CREATING DISKS FOR USE BY PERSISTENT
# VOLUMES, HENCE COMMENTED OUT BELOW
#kubectl apply -f ../resources/gce-ssd-storageclass.yaml

# Register GCE Fast SSD persistent disks and then create the persistent disks 
echo "Creating GCE disks(make sure the regions are same as cluster)"

gcloud compute disks create --size 30GB --type pd-ssd pd-ssd-disk-1
sleep 3

gcloud compute disks create --size 30GB --type pd-ssd pd-ssd-disk-2
sleep 3

gcloud compute disks create --size 30GB --type pd-ssd pd-ssd-disk-3
sleep 3

# Create persistent volumes using disks created above
echo "Creating GKE Persistent Volumes"

kubectl apply -f gce-ssd-persistentvolume1.yaml
sleep 3

kubectl apply -f gce-ssd-persistentvolume2.yaml
sleep 3

kubectl apply -f gce-ssd-persistentvolume3.yaml
sleep 3

# Create keyfile for the MongoD cluster as a Kubernetes shared secret
TMPFILE=$(mktemp)
/usr/bin/openssl rand -base64 741 > $TMPFILE
kubectl create secret generic shared-bootstrap-data --from-file=internal-auth-mongodb-keyfile=$TMPFILE
rm $TMPFILE

# Create mongodb service with mongod stateful-set
 kubectl apply -f mongodb-service.yaml

echo

# Wait until the final (3rd) mongod has started properly
echo "Waiting for the 3 containers to come up (`date`)..."
echo " (IGNORE any reported not found & connection errors)"
sleep 60
echo -n "  "
until kubectl --v=0 exec mongodb-service-2 -c mongod-container -- mongo --quiet --eval 'db.getMongo()'; do
    sleep 5
    echo -n "  "
done
echo "...mongod containers are now running (`date`)"
echo

# Print current deployment state
kubectl get persistentvolumes
echo
kubectl get all 
